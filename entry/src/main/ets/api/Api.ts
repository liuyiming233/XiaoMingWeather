import axios, { AxiosError, AxiosResponse } from '@ohos/axios'
import pako from 'pako'

export function requestGetWithGzip<T>(url: string,param: any): Promise<T> {
  return new Promise((resolve: Function, reject: Function) => {
    axios({
      method: 'get',
      url: url,
      params:param,
      responseType: 'array_buffer',
      headers: {
        'Accept-Encoding': 'gzip'
      }
    }).then(function (response) {
      if (response.status == 200) {
        const decompressedData = pako.inflate(response.data, { to: 'string' });
        resolve(JSON.parse(decompressedData))
      } else {
        reject("调用失败")
      }
    }).catch(function (error) {
      console.info("requestGetWithGzip:"+JSON.stringify(error));
      reject(error)
    });
  })
}

export async function requestPostWithGzip(url: string, param: any): Promise<any> {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: url,
      data: param,
      responseType: 'array_buffer',
      headers: {
        'Accept-Encoding': 'gzip'
      }
    }).then((response: AxiosResponse) => {
      if (response.status == 200) {
        const decompressedData = pako.inflate(response.data, { to: 'string' });
        resolve(JSON.parse(decompressedData))
      } else {
        reject("调用失败")
      }
    }).catch((error: AxiosError) => {
      console.info("requestPostWithGzip:"+JSON.stringify(error));
      reject(error)
    })
  })
}

export function requestGet<T>(url: string, param: any, gzip: boolean): Promise<T> {
  if (gzip) {
    return requestGetWithGzip<T>(url,param)
  } else {
    return new Promise((resolve, reject) => {
      axios.get(url,{params:param}).then(function (response) {
        if (response.status == 200) {
          resolve(response.data)
        } else {
          reject("调用失败")
        }
      }).catch(function (error) {
        console.info("requestGet:"+JSON.stringify(error));
        reject(error)
      });
    })
  }
}

export function requestPost<T>(url: string, param: any): Promise<T> {
  return new Promise((resolve, reject) => {
    axios.post(url, param).then((response: AxiosResponse) => {
      if (response.status == 200) {
        resolve(response.data)
      } else {
        reject("调用失败")
      }
    }).catch((error: AxiosError) => {
      console.info("requestPost:"+JSON.stringify(error));
      reject(error)
    })
  })
}

