import { ArrayUtil } from './ArrayUtil';
import { CharUtil } from './CharUtil'

export class StrUtil {
  /**
   *判断字符串是否为空白符(空白符包括空格、制表符、全角空格和不间断空格)true为空，否则false
   * @param str
   * @returns
   */
  static isBlank(str: string): boolean {
    let length: number;
    if ((str == null) || ((length = str.length) == 0)) {
      return true;
    }
    for (let i = 0; i < length; i++) {
      // 只要有一个非空字符即为非空字符串
      if (false == CharUtil.isBlankChar(str.charCodeAt(i))) {
        return false;
      }
    }

    return true;
  }

  /**
   *判断字符串是否为非空白符(空白符包括空格、制表符、全角空格和不间断空格)true为非空，否则false
   * @param str
   * @returns
   */
  static isNotBlank(str: string): boolean {
    return false == StrUtil.isBlank(str);
  }

  /**
   *
   * @param obj
   * @returns
   */
  static isBlankIfStr(obj: Object): boolean {
    if (null == obj) {
      return true;
    } else {
      return obj instanceof String ? StrUtil.isBlank(String(obj)) : false;
    }
  }

  /**
   *
   * @param obj
   * @returns
   */
  static isEmptyIfStr(obj: Object): boolean {
    if (null == obj) {
      return true;
    } else if (obj instanceof String) {
      return 0 == (obj).length;
    } else {
      return false;
    }
  }

  /**
   *去除传入集合的每个值的前后空格
   * @param strs
   * @returns
   */
  static trim(strs: String[]): String[] {
    return strs.map((value) => value.trim());
  }

  /**
   * 判断传入的字符串中是否包含有空值,只要有一个则返回true,否则false
   * @param strs 字符串列表
   * @return 是否包含空字符串
   */
  static hasBlank(...strs: string[]): boolean {
    if (ArrayUtil.strValIsEmpty(strs)) {
      return true;
    }
    for (let str of strs) {
      if (StrUtil.isBlank(str)) {
        return true;
      }
    }
    return false;
  }

  /**
   *  字符串是否为空
   * @param str 被检测的字符串
   * @return 是否为空
   */
  static isEmpty(str: string): boolean {
    return str == null || str.length == 0;
  }


  /**
   * 转换字符串首字母为大写，剩下为小写
   * @param str 待转换的字符串
   * @returns 转换后的
   */
  static capitalize(str: string = ''): string {
    if (!str) {
      return '';
    }
    const firstChar = str.charAt(0).toUpperCase();
    const restChars = str.slice(1).toLowerCase();
    return firstChar + restChars;
  }

  /**
   * 检查字符串是否以给定的字符串结尾
   * @param string 要检索的字符串
   * @param target 要检索字符
   * @param position 检索的位置
   * @returns 如果字符串以字符串结尾，那么返回 true，否则返回 false
   */
  static endsWith(string: string = '', target: string, position: number = string.length): boolean {
    return string.endsWith(target, position);
  }

  /**
   * 重复 N 次给定字符串
   * @param str  要重复的字符串
   * @param n  重复的次数
   * @returns
   */
  static repeat(str: string = '', n: number = 1): string {
    return str.repeat(n);
  }

  /**
   * 替换字符串中匹配的正则为给定的字符串
   * @param str   待替换的字符串
   * @param pattern  要匹配的内容正则或字符串
   * @param replacement 替换的内容
   * @returns 返回替换后的字符串
   */
  static replace(str: string = '', pattern: RegExp | string, replacement: string): string {
    return str.replace(pattern, replacement);
  }

  /**
   * 检查字符串是否以给定的字符串卡头
   * @param string 要检索的字符串
   * @param target 要检索字符
   * @param position 检索的位置
   * @returns 如果字符串以字符串开头，那么返回 true，否则返回 false
   */
  static startsWith(string: string = '', target: string, position: number = 0): boolean {
    return string.startsWith(target, position);
  }

  /**
   * 转换整个字符串的字符为小写
   * @param str  要转换的字符串
   * @returns 返回小写的字符串
   */
  static toLower(str: string = ''): string {
    return str.toLowerCase();
  }

  /**
   * 转换整个字符串的字符为大写
   * @param str  要转换的字符串
   * @returns 返回小写的字符串
   */
  static toUpper(str: string = ''): string {
    return str.toUpperCase();
  }
}