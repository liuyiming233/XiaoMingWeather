import relationalStore from '@ohos.data.relationalStore';
import hilog from '@ohos.hilog';
import common from '@ohos.app.ability.common';
import util from '@ohos.util';
import { StrUtil} from '../common/utils/StrUtil'
import router from '@ohos.router';
import web_webview from '@ohos.web.webview';
import {CityInfo} from '../model/CityInfo';
import { ValuesBucket } from '@ohos.data.ValuesBucket';
import { BusinessError } from '@ohos.base';
import { GlobalContext } from '../common/utils/GlobalContext';
import promptAction from '@ohos.promptAction';

// 统一的卡片样式
@Styles function card(){
  .width('95%')
  .padding(20)
  .backgroundColor("#43749F")
  .borderRadius(15)
}

@Entry
@Component
struct CityPage {
  @State changeValue: string = ''
  @State submitValue: string = ''
  @State searchList: CityInfo[] = []

  @State type: string = '1'
  @State openType: string = 'open'
  @State latitude: string = ''
  @State longitude: string = ''
  @State locality: string = ''
  @State subLocality: string = ''

  @StorageLink('statusHeight') statusHeight: string = '126px';
  @StorageLink('navigationHeight') navigationHeight: string = '40px';

  controller: SearchController = new SearchController()

  context = getContext(this) as common.UIAbilityContext;
  STORE_CONFIG: relationalStore.StoreConfig = {
    name: 'XiaoMingWeather.db', // 数据库文件名
    securityLevel: relationalStore.SecurityLevel.S1 // 数据库安全级别
  };

  async aboutToAppear() {
    let params = router.getParams() as Record<string, string>;
    if (params != null && params != undefined) {
      this.openType = params.openType;
    }
    let store = GlobalContext.getContext().get('store') as relationalStore.RdbStore;
    store.querySql("SELECT * FROM CITY_INFO", []).then(async resultSet => {
      if (resultSet.rowCount == 0) {
        let rawFile = this.context.resourceManager.getRawFileContentSync('city.json');
        let decoder = util.TextDecoder.create('UTF-8');
        let text = decoder.decodeWithStream(rawFile);
        let cityArray = JSON.parse(text) as ValuesBucket[];
        hilog.info(0x0001, 'city', '%{public}s', `获取city.json文件数据：共计:${cityArray.length}条`);
        store.batchInsert("CITY_INFO", cityArray);
      }
    })
  }

  async queryCity(param:string){
    if (StrUtil.isBlank(param)) {
      this.searchList = []
      return;
    }
    let name = '%' + param + '%'
    let store = GlobalContext.getContext().get('store') as relationalStore.RdbStore;
    let sql = "SELECT * FROM CITY_INFO WHERE Location_Name_ZH LIKE ? OR Location_Name_EN LIKE ? " +
      "OR Adm1_Name_EN LIKE ? OR Adm1_Name_ZH LIKE ? OR Adm2_Name_EN LIKE ? OR Adm2_Name_ZH LIKE ?";
    let cityList = await store.querySql(sql, [name,name,name,name,name,name]).then(resultSet => {
      let arr: CityInfo[] = []
      if (resultSet.rowCount > 0) {
        for (let i = 0; i < resultSet.rowCount; i++) {
          let success = resultSet.goToRow(i)
          if (success == true) {
            let result:CityInfo = new CityInfo();
            for (let j = 0; j < resultSet.columnNames.length; j++) {
              let column = resultSet.columnNames[j];
              let index = resultSet.getColumnIndex(column);
              if (column === 'Latitude') {
                result.Latitude = resultSet.getDouble(index);
              }
              if (column === 'Longitude') {
                result.Longitude = resultSet.getDouble(index);
              }
              if (column === 'Location_ID') {
                result.Location_ID = resultSet.getString(index);
              }
              if (column === 'Location_Name_ZH') {
                result.Location_Name_ZH = resultSet.getString(index);
              }
              if (column === 'Country_Region_ZH') {
                result.Country_Region_ZH = resultSet.getString(index);
              }
              if (column === 'Adm1_Name_ZH') {
                result.Adm1_Name_ZH = resultSet.getString(index);
              }
              if (column === 'Adm2_Name_ZH') {
                result.Adm2_Name_ZH = resultSet.getString(index);
              }
            }
            arr.push(result)
          }
        }
      }
      return arr;
    })
    this.searchList = cityList
  }

  async routePage(item:CityInfo) {
    if (this.openType == 'open'){
      try {
        this.changeValue = '';
        await router.pushUrl({
          url: 'pages/QueryWeatherPage',
          params: {
            type: '2',
            latitude: item.Latitude,
            longitude: item.Longitude,
            adm1NameZh: item.Adm1_Name_ZH,
            locality: item.Adm2_Name_ZH,
            subLocality: item.Location_Name_ZH,
            locationId: item.Location_ID,
          }
        }, router.RouterMode.Single)
      } catch (err) {
        console.info(` fail callback, code: ${err.code}, msg: ${err.msg}`)
      }
    } else {
      let store = GlobalContext.getContext().get('store') as relationalStore.RdbStore;
      store.querySql("SELECT * FROM CITY_WEATHER WHERE Location_ID = ?", [item.Location_ID]).then(resultSet => {
        if (resultSet.rowCount == 0) {
          let param: ValuesBucket = {
            Location_ID: item.Location_ID,
            Location_Name_ZH: item.Location_Name_ZH,
            Adm1_Name_ZH: item.Adm1_Name_ZH,
            Adm2_Name_ZH: item.Adm2_Name_ZH,
            Latitude: item.Latitude,
            Longitude: item.Longitude
          }
          store.batchInsert("CITY_WEATHER", [param])
          try {
            this.changeValue = '';
            router.back()
          } catch (err) {
            console.info(` fail callback, code: ${err.code}, msg: ${err.msg}`)
            promptAction.showToast({
              message: '系统开小差了',
              duration: 2000,
            });
          }
        }
      })
    }

  }

  build() {
    Column() {
      Row({space:10}){
        Button() {
          Image($rawfile('icon/back.svg')).width(25).height(25).fillColor('#f5f5f5')
        }
        .backgroundColor('#00ffffff')
        .fontSize(20)
        .fontWeight(FontWeight.Bolder)
        .onClick(() => {
          try {
            router.back()
          } catch (err) {
            console.info(` fail callback, code: ${err.code}, msg: ${err.msg}`)
          }
        })

        Search({ value: this.changeValue, placeholder: '搜索城市(中文/拼音)', controller: this.controller })
          .searchButton('搜索',{fontColor:'#f5f5f5'})
          .height(40)
          .layoutWeight(1)
          .backgroundColor('#43749F')
          .placeholderColor('#B3C4D6')
          .placeholderFont({ size: 14, weight: 400 })
          .textFont({ size: 14, weight: 400 })
          .fontColor('#F5F5F5')
          .caretStyle({
            color: '#fff'
          })
          .searchIcon({
            color: '#F5F5F5'
          })
          .onSubmit((value: string) => {
            this.changeValue = value
            this.queryCity(value)
          })
          .onChange((value: string) => {
            this.changeValue = value
            this.queryCity(value)
          })

      }.height(50).width('100%')
      .padding({left:20,right:20})
      .alignItems(VerticalAlign.Center)
      .justifyContent(FlexAlign.SpaceBetween)
      if (this.searchList.length > 0) {
        Column(){
          List({space:20}){
            ForEach(this.searchList,(item:CityInfo)=>{
              ListItem(){
                Row(){
                  Text(item.Location_Name_ZH + ',' + item.Adm1_Name_ZH +','+ item.Adm2_Name_ZH)
                    .fontColor('#f5f5f5')
                    .fontSize(20)
                }
                .width('100%').justifyContent(FlexAlign.Start)
                .zIndex(0)
                .onClick(()=>{
                  this.routePage(item);
                })
              }
            })
          }.alignListItem(ListItemAlign.Start)
          .divider({ strokeWidth: 1, color: '#6f868585' }) // 每行之间的分界线
        }.card()
        .layoutWeight(1)
        .margin({top:20,bottom:20})
      }
    }
    .justifyContent(FlexAlign.Start)
    .backgroundColor('#326799')
    .width('100%')
    .height('100%')
    .padding({top: this.statusHeight})

  }
}