import display from '@ohos.display';
import RefreshModel from '../model/RefreshModel';
import { Daily } from '../model/Weather';
import dayjs from 'dayjs';
import image from '@ohos.multimedia.image';
import common from '@ohos.app.ability.common';

@Component
export struct CurveComponent {

  @Consume refreshModel:RefreshModel

  private settings: RenderingContextSettings = new RenderingContextSettings(true)
  private context: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.settings)

  @State canvasWidth: number= this.refreshModel.daily.length * 60
  @State pmWidth: number = px2vp(display.getDefaultDisplaySync().width) - 40

  @State minimum: number = 0
  @State maximum: number = 100
  @State total: number = 0

  @State inertia: number = 0
  @State velocityX: number = 0
  @State offsetX: number = 0
  @State offsetY: number = 0
  @State positionX: number = 0
  @State positionY: number = 0

  @State paddingTop: number = 100
  @State paddingBottom: number = 110

  @State pixelMapDay:PixelMap[] = []
  @State pixelMapNight:PixelMap[] = []

  private nowTime:number = new Date().getTime()
  private panOption: PanGestureOptions = new PanGestureOptions({ direction: PanDirection.Left | PanDirection.Right })
  private pageContext = getContext(this) as common.UIAbilityContext;

  async getData(){

    //this.pmWidth = px2vp(display.getDefaultDisplaySync().width) - 40
    //this.canvasWidth = this.refreshModel.daily.length * 63

    let tempaArr:number[] = [];

    let imageArr:PixelMap[] = [];
    let imageArr2:PixelMap[] = [];

    for (let i = 0; i < this.refreshModel.daily.length; i++) {
      let item = this.refreshModel.daily[i]
      tempaArr.push(parseFloat(item.tempMax))
      tempaArr.push(parseFloat(item.tempMin))

      let fileData = await this.pageContext.resourceManager.getRawFileContent('QWeather/image/' + item.iconDay + '-fill.png')
      // 通过缓冲区创建图片源实例
      const imageSource = image.createImageSource(fileData.buffer.slice(0));
      // 通过图片解码参数创建PixelMap对象
      let pixelmap = await imageSource.createPixelMap()
      imageArr.push(pixelmap)

      let fileData2 = await this.pageContext.resourceManager.getRawFileContent('QWeather/image/' + item.iconNight + '-fill.png')
      // 通过缓冲区创建图片源实例
      const imageSource2 = image.createImageSource(fileData2.buffer.slice(0));
      // 通过图片解码参数创建PixelMap对象
      let pixelmap2 = await imageSource2.createPixelMap()
      imageArr2.push(pixelmap2)
    }

    this.pixelMapDay.push(...imageArr)
    this.pixelMapNight.push(...imageArr2)

    let min = Math.min(...tempaArr)
    if (min > 0) {
      this.minimum = min - min * 0.2
    } else {
      if (min == 0) {
        this.minimum = - 2
      } else {
        this.minimum = min + min * 0.2
      }
    }

    let max = Math.max(...tempaArr)
    if (max >= 0) {
      if (max == 0) {
        this.maximum = 2
      } else {
        this.maximum = max + max * 0.2
      }
    } else {
      this.maximum = max - max * 0.2
    }
    this.total = Math.abs(this.maximum) + Math.abs(this.minimum)
    console.log("最大差值" + this.total)
    this.canvasReady();
  }

  canvasReady(){
    let width = this.context.width;
    let height = this.context.height - this.paddingTop - this.paddingBottom;

    // X轴所需绘制点的横向间隔
    let xInterval = width / this.refreshModel.daily.length;

    //X轴上的高度（正数）
    let upHeight = 0;
    let downHeight = 0;
    if (this.maximum > 0 && this.minimum <= 0) {
      upHeight = height * Math.abs(this.maximum)/(Math.abs(this.maximum) + Math.abs(this.minimum))
      downHeight = height * Math.abs(this.minimum)/(Math.abs(this.maximum) + Math.abs(this.minimum))
    } else if (this.maximum > 0 && this.minimum > 0) {
      upHeight = height;
    } else {
      downHeight = height
    }

    //用于在图表中计算y值的函数
    let gety = (d: number) => {
      if (d > 0){
        return upHeight * (1- d / this.maximum) + this.paddingTop ;
      } else {
        return downHeight * (Math.abs(d) / Math.abs(this.minimum)) + upHeight + this.paddingTop
      }
    };
    //用于在图标中计算x值的函数
    let getx: (i: number) => number = (i) => {
      //向右偏移  量
      let p: number = xInterval / 3;
      return p + xInterval * i
    };

    // 设置绘制曲线图的颜色
    this.context.lineWidth = 2;
    this.context.strokeStyle = "#d5e2e2e2";

    this.context.fillStyle = '#fff'
    this.context.font = '40px'

    let lastY = 0;
    this.context.beginPath();
    this.refreshModel.daily.forEach((value: Daily, index: number) => {
      //如果没有超过绘制的最大数目
      let x = getx(index);
      let y = gety(parseFloat(value.tempMax));
      let x1 = x - xInterval / 2;
      let y1 = lastY;
      let x2 = x + -xInterval / 2;
      let y2 = y;
      if (index == 0)
        this.context.moveTo(x, y);
      else
        this.context.bezierCurveTo(x1, y1, x2, y2, x, y)
      lastY = y

      this.context.textAlign = 'center'
      this.context.fillText(value.tempMax + '°', x, y - 5)
      this.context.fillText(this.refreshModel.weekdays[dayjs(this.refreshModel.daily[index].fxDate).day()], x, 20)
      this.context.fillText(dayjs(this.refreshModel.daily[index].fxDate).format('MM/DD'), x, 40)
      this.context.fillText(this.refreshModel.daily[index].textDay, x, 90)
      this.context.fillText(this.refreshModel.daily[index].textNight, x, 220)
      this.context.drawImage(this.pixelMapDay[index], x-10, 50, 20, 20)
      this.context.drawImage(this.pixelMapNight[index], x-10, 230, 20, 20)

      let sunrise = new Date(this.refreshModel.daily[index].fxDate + " " + this.refreshModel.daily[index].sunrise).getTime();
      let sunset = new Date(this.refreshModel.daily[index].fxDate + " " + this.refreshModel.daily[index].sunset).getTime();
      if (this.nowTime > sunrise &&  this.nowTime < sunset) {
        this.context.fillText(this.refreshModel.daily[index].windDirDay, x, 270)
        this.context.fillText(this.refreshModel.daily[index].windScaleDay + '级', x, 290)
      } else {
        this.context.fillText(this.refreshModel.daily[index].windDirNight, x, 270)
        this.context.fillText(this.refreshModel.daily[index].windScaleNight + '级', x, 290)
      }
    });
    // 结束绘制折线图
    this.context.stroke();

    this.context.beginPath();
    lastY = 0;
    this.refreshModel.daily.forEach((value: Daily, index: number) => {
      //如果没有超过绘制的最大数目
      let x = getx(index);
      let y = gety(parseFloat(value.tempMin));
      let x1 = x - xInterval / 2;
      let y1 = lastY;
      let x2 = x + -xInterval / 2;
      let y2 = y;
      if (index == 0)
        this.context.moveTo(x, y);
      else
        this.context.bezierCurveTo(x1, y1, x2, y2, x, y)
      lastY = y

      this.context.textAlign = 'center'
      this.context.fillText(value.tempMin + '°', x, y + 15)
    });
    // 结束绘制折线图
    this.context.stroke();

    this.context.strokeStyle = "#ffffffff";
    this.context.fillStyle = '#ffffffff';
    this.refreshModel.daily.forEach((value: Daily, index: number) => {
      //如果没有超过绘制的最大数目
      let x = getx(index);
      let y = gety(parseFloat(value.tempMin));

      this.context.beginPath();
      this.context.lineCap = 'round';
      this.context.arc(x, y, 2, 0, Math.PI * 2)
      this.context.fill()
      this.context.stroke();

      let x2 = getx(index);
      let y2 = gety(parseFloat(value.tempMax));

      this.context.beginPath();
      this.context.lineCap = 'round';
      this.context.arc(x2, y2, 2, 0, Math.PI * 2)
      this.context.fill()
      this.context.stroke();

    });
  }

  build() {

    Column(){
      Column(){
        Text('未来预报').fontSize(15).fontColor('#ffd9d9d9')
      }.alignItems(HorizontalAlign.Start)
      .width('100%')

      Stack(){
        Canvas(this.context)
          .width(this.canvasWidth)
          .height(300)
          .onReady(()=>{
            this.getData()
          })
          .translate({ x: this.offsetX, y: 0, z: 0 })// 以组件左上角为坐标原点进行移动
          .gesture(
            PanGesture(this.panOption)
              .onActionStart((event: GestureEvent) => {
              })
              .onActionUpdate((event: GestureEvent) => {
                //console.log(event.velocityX.toString() + ' ' + event.offsetX)
                let offsetX = 0;
                if (event.offsetX < 0 && Math.abs(this.positionX + event.offsetX) <= (this.canvasWidth - this.pmWidth + 10)) {
                  offsetX = this.positionX + event.offsetX;
                } else if (event.offsetX > 0){
                  if (this.positionX + event.offsetX > 0) {
                    offsetX = 0;
                  } else {
                    offsetX = this.positionX + event.offsetX;
                  }
                } else {
                  offsetX = -(this.canvasWidth - this.pmWidth + 10);
                }
                this.offsetX = offsetX;
                this.velocityX = event.velocityX;
              })
              .onActionEnd(() => {
                //惯性 速度200以内没有惯性，大于200时每增加100速度 惯性增加30
                this.inertia = 0;
                if (Math.abs(this.velocityX) > 200) {
                  this.inertia = (Math.abs(this.velocityX) - 200) / 100 * 30
                }
                let offsetX = 0;
                if (this.velocityX < 0 && Math.abs(this.offsetX - this.inertia) <= (this.canvasWidth - this.pmWidth + 10)) {
                  offsetX = this.offsetX - this.inertia;
                } else if (this.velocityX > 0){
                  if (this.velocityX > 0 && this.offsetX + this.inertia > 0) {
                    offsetX = 0;
                    this.inertia = 0;
                  } else {
                    offsetX = this.offsetX + this.inertia;
                  }
                } else {
                  offsetX = -(this.canvasWidth - this.pmWidth + 10);
                  this.inertia = 0;
                }
                this.positionX = offsetX
                animateTo({ duration: 400, curve: Curve.LinearOutSlowIn }, () => {
                  this.offsetX = offsetX;
                })
              })
          )

      }.width('100%')
      .alignContent(Alignment.Start)

    }.width("100%")
    .clip(true)

  }
}