import { common } from '@kit.AbilityKit';
import { image } from '@kit.ImageKit';
import RefreshModel from '../model/RefreshModel';

class point{
  x:number = 0;
  y:number = 0;
  constructor(x:number,y:number) {
    this.x = x;
    this.y = y;
  }
}

class imgPoint {
  x: number = 0;
  y: number = 0;
  angle: number = 0;
  wind: string = '';

  constructor(x: number, y: number, angle: number, wind: string) {
    this.x = x;
    this.y = y;
    this.angle = angle;
    this.wind = wind;
  }
}

@Component
export struct WindComponent {

  @Consume refreshModel: RefreshModel
  @State sun:PixelMap | null = null;
  @State sunrise:string = '';
  @State sunset:string = '';
  @State arrow:PixelMap | null = null;

  private settings: RenderingContextSettings = new RenderingContextSettings(true)
  private context: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.settings)

  private pageContext = getContext(this) as common.UIAbilityContext;

  async canvasReady(){
    let width = this.context.width;
    let height = this.context.height;
    this.context.clearRect(0, 0, width, height)
    console.log(width + ' ' + height)

    let outRadius = (height - 10) / 2;
    let inRadius = (height - 10) / 2 - 6;
    let inRadius2 = (height - 10) / 2 - 8;

    let center:point = new point(width/2, outRadius + 5)

    const numPoints = 60;
    const angleIncrement = 2 * Math.PI / numPoints; // 计算每个点的角度增量

    let angle = 0;
    let outPoints:point[] = []; // 用于存储坐标点的数组
    let inPoints:point[] = []; // 用于存储坐标点的数组
    let inPoints2:point[] = []; // 用于存储坐标点的数组
    for (let i = 0; i < numPoints; i++) {
      outPoints.push(this.calculatePoints(center, outRadius, angle));
      inPoints.push(this.calculatePoints(center, inRadius, angle));
      inPoints2.push(this.calculatePoints(center, inRadius2, angle));
      angle += angleIncrement; // 增加角度
    }

    this.context.lineWidth = 1
    this.context.strokeStyle = '#ffe2e2e2'
    for (let i = 0; i < outPoints.length; i++) {
      this.context.beginPath();
      this.context.moveTo(outPoints[i].x,outPoints[i].y);
      if (i % 5 == 0) {
        this.context.lineWidth = 2
        this.context.lineTo(inPoints2[i].x,inPoints2[i].y)
      } else {
        this.context.lineWidth = 1
        this.context.lineTo(inPoints[i].x,inPoints[i].y)
      }
      this.context.stroke();
    }

    let arrowData = this.pageContext.resourceManager.getRawFileContentSync('image/arrow.png')
    // 通过缓冲区创建图片源实例
    const imageSource = image.createImageSource(arrowData.buffer.slice(0));
    // 通过图片解码参数创建PixelMap对象
    this.arrow = await imageSource.createPixelMap()

    let arr:imgPoint[] = [];
    arr.push(new imgPoint(inPoints[0].x, inPoints[0].y, Math.PI / 180 * 90,'东风'))

    let p45 = this.calculatePoints(center, inRadius, Math.PI / 180 * 45);
    arr.push(new imgPoint(p45.x,p45.y, Math.PI / 180 * 135, '东南风'))

    arr.push(new imgPoint(inPoints[15].x,inPoints[15].y, Math.PI / 180 * 180, '南风'))

    let p135 = this.calculatePoints(center, inRadius, Math.PI / 180 * 135);
    arr.push(new imgPoint(p135.x, p135.y, Math.PI / 180 * 225, '西南风'))

    arr.push(new imgPoint(inPoints[30].x,inPoints[30].y, Math.PI / 180 * 270, '西风'))

    let p225 = this.calculatePoints(center, inRadius, Math.PI / 180 * 225);
    arr.push(new imgPoint(p225.x, p225.y, Math.PI / 180 * 315, '西北风'))

    arr.push(new imgPoint(inPoints[45].x,inPoints[45].y, 0, '北风'))

    let p315 = this.calculatePoints(center, inRadius, Math.PI / 180 * 315);
    arr.push(new imgPoint(p315.x, p315.y, Math.PI / 180 * 45, '东北风'))


    for (let i = 0; i < arr.length; i++) {
      if (this.refreshModel.now.windDir === arr[i].wind) {
        this.context.save()
        this.context.translate(arr[i].x, arr[i].y)
        this.context.rotate(arr[i].angle)
        this.context.translate(-(arr[i].x), -(arr[i].y))
        this.context.drawImage(this.arrow, arr[i].x - 10, arr[i].y - 11, 20, 20)
        this.context.restore()
      }
    }
    this.context.fillStyle = '#fff'
    this.context.font = 'bolder 28vp'
    this.context.textAlign = 'center'
    this.context.fillText(this.refreshModel.now.windScale + ' 级', center.x,center.y - 5)
    this.context.font = '15vp'
    this.context.fillStyle = '#ffc4c4c4'
    this.context.fillText(this.refreshModel.now.windDir, center.x,center.y + 30)

  }

  calculatePoints(center:point,radius:number,angle:number){
    let x = 0;
    let y = 0;
    y = center.y + radius * Math.sin(angle);
    x = center.x + radius * Math.cos(angle);
    return new point(x,y);
  }

  build() {

    Column(){
      Column(){
        Text('风').fontSize(15).fontColor('#ffd9d9d9')
      }.alignItems(HorizontalAlign.Start)
      .width('100%')
      Canvas(this.context)
        .width('100%')
        .layoutWeight(1)
        .backgroundColor('#00ffffff')
        .onReady(()=>{
          this.canvasReady();
        })
      Row(){
        Text(this.refreshModel.now.windSpeed + '公里/小时').fontColor('#FFF').fontSize(12)
      }.width('100%')
      .justifyContent(FlexAlign.Start)
    }
    //.backgroundColor('#FFF')
  }
}